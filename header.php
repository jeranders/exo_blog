<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Mon Blog</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">
                        <?php if ($articleManager->countArticle() == 0){ ?>
                            Il y a <?= $articleManager->countArticle(); ?> article.
                        <?php }else{ ?>
                            Il y a <?= $articleManager->countArticle(); ?> articles.
                        <?php } ?>
                    </a>
                </li>
                <li><a href="index.php">Accueil</a></li>
                <?php if(isset($_SESSION['id_membre']) == '') { ?>
                    <li><a href="login.php">Connexion</a></li>
                    <li><a href="register.php">Inscription</a></li>
                <?php }else{ ?>
                    <?php if ($membreManager->getMembre($_SESSION['id_membre'])->getRang() == 1) { ?>
                        <li><a href="admin.php">Administration</a></li>
                    <?php } ?>
                    <li><a href="logout.php">Deconnexion</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>