<?php
include 'bdd_autoload.php';
$App->verifSession();
/**
 * Script de connexion
 */
if (isset($_POST['sendConnexion']))
{
    $connexion = new Membre([
        'pseudo' => htmlentities($_POST['pseudoConnexion']),
        'password' => $_POST['passwordConnexion']
    ]);

    $membreManager->connexion($connexion);
}

include 'header.php';
?>
    <div class="container">

        <div class="row">

            <div class="col-md-6 col-md-offset-3">
                <?php
                if (isset($_SESSION['erreursConnexion']))
                {
                    $App->erreursSession($_SESSION['erreursConnexion']);
                    unset($_SESSION['erreursConnexion']);
                }
                ?>
                <h1>Connexion</h1>

                <form action="#" method="post">

                    <div class="form-group">
                        <label for="pseudoConnexion">Pseudo</label>
                        <input type="text" class="form-control" id="pseudoConnexion" name="pseudoConnexion" placeholder="Ex : Toto81">
                    </div>

                    <div class="form-group">
                        <label for="passwordConnexion">Mot de passe</label>
                        <input type="password" class="form-control" id="passwordConnexion" name="passwordConnexion" placeholder="Password">
                    </div>

                    <button type="submit" name="sendConnexion" class="btn btn-default">Valider</button>

                </form>

            </div>

        </div>
    </div>
<?php include 'footer.php'; ?>