<?php
include 'bdd_autoload.php';
/**
 * Ajout d'un article
 */
if (isset($_POST['ajoutArticle']))
{
    $_SESSION['erreurArticle'] = [];
    if (strlen($_POST['titre']) <= 2)
    {
        $_SESSION['erreurArticle'][] = 'Le titre est trop court';
    }

    if (strlen($_POST['auteur']) <= 2)
    {
        $_SESSION['erreurArticle'][] = 'Le nom de l\'auteur est trop court';
    }

    if (strlen($_POST['contenu']) <= 99)
    {
        $_SESSION['erreurArticle'][] = 'Le contenu est trop court';
    }

    if (count($_SESSION['erreurArticle']) == 0)
    {
        $article = new Article([
            'titre' => htmlentities($_POST['titre']),
            'contenu' => htmlentities($_POST['contenu']),
            'auteur' => htmlentities($_POST['auteur'])
        ]);
        $articleManager->addArticle($article);
    }


}
include 'header.php';
?>
    <div class="container">
        <?php
        if (isset($_SESSION['id_membre']) != '')
        {
            if ($membreManager->getMembre($_SESSION['id_membre'])->getRang() == 1)
            {
                ?>
                <div class="row">
                    <div class="col-md-6 col-md-offset-5">
                        <button type="button" class="btn btn-primary navbar-btn" data-toggle="modal" data-target="#ajoutArticle">Ajouter un article</button>
                    </div>
                </div>
                <?php
            }
        }
        
        ?>

        <div class="row">

            <div class="col-md-8 col-md-offset-2">
                <?php
                if (isset($_SESSION['msgSuccess']))
                {
                    $App->successSession($_SESSION['msgSuccess']);
                    unset($_SESSION['msgSuccess']);
                }
                ?>
                <?php
                if (isset($_SESSION['erreurArticle']))
                {
                    $App->erreursSession($_SESSION['erreurArticle']);
                    unset($_SESSION['erreurArticle']);
                }
                ?>
                <?php if ($articleManager->countArticle() != 0){
                    foreach ($articleManager->getListArticles() as $articles) { ?>
                        <div class="article">
                            <h3><a href="article.php?id=<?= $articles->getId(); ?>"><?= html_entity_decode($articles->getTitre()); ?></a></h3>

                            <ul class="list-unstyled list-inline">
                                <li><span class="glyphicon glyphicon-calendar" aria-hidden="true"> <?= $articles->getDate(); ?></li>
                                <li><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?= htmlentities($articles->getAuteur()); ?></li>
                                <li><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> (<?= $commentaireManager->countCommentaires($articles->getId()); ?>)</li>
                            </ul>

                            <div class="article-content">
                                <?php

                                $tailleArticle = strlen($articles->getContenu());

                                if ($tailleArticle >= 600) { ?>

                                    <?= html_entity_decode(mb_substr($articles->getContenu(),0 ,600)); ?>... <a href="article.php?id=<?= $articles->getId(); ?>">Voir la suite</a>

                                <?php }else { ?>
                                    <?= html_entity_decode($articles->getContenu()); ?>
                                <?php  } ?>

                            </div>

                        </div>

                    <?php }
                }else{ ?>
                    <div class="text-center">Aucun article dans la base de données</div>
                <?php } ?>


            </div>

        </div>
    </div>
<?php include 'footer.php'; ?>