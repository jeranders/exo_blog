<?php include 'bdd_autoload.php'; ?>
<?php
$_GET['id'] = (int) $_GET['id'];
$id = $_GET['id'];

if (isset($_GET['id']) != $_GET['id'])
{
    header('Location: index.php');
}

/**
 * Supprime l'article
 */
if (isset($_GET['del']) && $_GET['id'] == $id)
{
    $idDelete = (int) $_GET['del'];
    $articleManager->deleteArticle($idDelete);
}

/**
 * Ajout de commentaire
 */
if (isset($_POST['sendCommentaire']))
{
    $_SESSION['erreurCommentaire'] = [];

    if (strlen($_POST['auteur']) <= 2)
    {
        $_SESSION['erreurCommentaire'][] = 'Le nom de l\'auteur est trop court';
    }

    if (strlen($_POST['commentaire']) <= 100)
    {
        $_SESSION['erreurCommentaire'][] = 'Le contenu est trop court';
    }

    if (count($_SESSION['erreurCommentaire']) == 0)
    {
        $commentaire = new Commentaire([
            'auteur' => htmlentities($_POST['auteur']),
            'commentaire' => htmlentities($_POST['commentaire']),
            'id_article' => (int) $id
        ]);
        $commentaireManager->addCommentaire($commentaire);
    }
}

/**
 * Modifie l'article
 */
if (isset($_POST['modif']))
{
    $_SESSION['erreurModif'] = [];

    if (strlen($_POST['titre']) <= 2)
    {
        $_SESSION['erreurModif'][] = 'Le titre est trop court';
    }

    if (strlen($_POST['auteur']) <= 2)
    {
        $_SESSION['erreurModif'][] = 'Le nom de l\'auteur est trop court';
    }

    if (strlen($_POST['contenu']) <= 99)
    {
        $_SESSION['erreurModif'][] = 'Le contenu est trop court';
    }

    if (count($_SESSION['erreurModif']) == 0)
    {
        $article = new Article([
            'titre' => htmlentities($_POST['titre']),
            'contenu' => htmlentities($_POST['contenu']),
            'auteur' => htmlentities($_POST['auteur']),
            'id' => (int) $id
        ]);
        $idModif = (int) $_POST['modif'];
        $articleManager->updateArticle($article);
    }
}

/**
 * Supprimer les commentaires
 */
if (isset($_GET['commentaireDel']) && $_GET['id'] == $id)
{
    $idDelete = (int) $_GET['commentaireDel'];
    $commentaireManager->deleteCommentaire($idDelete, $id);
}

/**
 * Moderation spam commentaires
 */
if (isset($_GET['moderationSpam']) && $_GET['id'] == $id)
{
    $idModeration = (int) $_GET['moderationSpam'];
    $commentaireManager->moderationSpam($idModeration, CommentairesManager::MODERATION_SPAM, $id);
}

/**
 * Moderation Insulte commentaires
 */
if (isset($_GET['moderationInsulte']) && $_GET['id'] == $id)
{
    $idModeration = (int) $_GET['moderationInsulte'];
    $commentaireManager->moderationSpam($idModeration, CommentairesManager::MODERATION_INSULTE, $id);
}
?>
<?php include 'header.php'; ?>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <?php
                if (isset($_SESSION['erreurModif']))
                {
                    $App->erreurs($_SESSION['erreurModif']);
                    unset($_SESSION['erreurModif']);
                }
                ?>

                <div class="article">
                    <h1><?= $articleManager->getArticle($id)->getTitre(); ?></h1>

                    <ul class="list-unstyled list-inline">
                        <li><span class="glyphicon glyphicon-calendar" aria-hidden="true"> <?= $articleManager->getArticle($id)->getDate(); ?></li>
                        <li><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?= htmlentities($articleManager->getArticle($id)->getAuteur()); ?></li>
                        <?php if(isset($_SESSION['id_membre']) != '') { ?>
                            <?php if ($membreManager->getMembre($_SESSION['id_membre'])->getRang() == 1) { ?>
                                <li><a href="article.php?id=<?= $articleManager->getArticle($id)->getId(); ?>&del=<?= $articleManager->getArticle($id)->getId(); ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></li>
                                <li>
                                    <button type="button" class="btn btn-success navbar-btn" data-toggle="modal" data-target="#modifArticle"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>
                                </li>
                            <?php } ?>
                        <?php } ?>

                    </ul>

                    <div class="article-content">
                        <?= html_entity_decode($articleManager->getArticle($id)->getContenu()); ?>
                    </div>

                </div>
            </div>

            <div class="col-md-8 col-md-offset-2">
                <h3>Les commentaires (<?= $commentaireManager->countCommentaires($id); ?>)</h3>
                <?php
                if (isset($_SESSION['erreurCommentaire']))
                {
                    $App->erreurs($_SESSION['erreurCommentaire']);
                    unset($_SESSION['erreurCommentaire']);
                }

                if (isset($_SESSION['delCommentaire']))
                {
                    $App->successSession($_SESSION['delCommentaire']);
                    unset($_SESSION['delCommentaire']);
                }
                ?>
                <form action="#" method="post">
                    <div class="form-group">
                        <label>Auteur</label>
                        <input type="text" class="form-control" id="auteurCommentaire" placeholder="Auteur" name="auteur" value="<?php if (isset($_POST['auteur'])){echo $_POST['auteur'];}?>">
                        <span class="msg-erreur" id="auteurCommentaireMsg"></span>
                    </div>
                    <div class="form-group">
                        <label>Commentaire</label>
                        <textarea class="form-control" rows="3" id="commentaireTexte" name="commentaire"><?php if (isset($_POST['commentaire'])){echo $_POST['commentaire'];}?></textarea>
                        <span class="msg-erreur" id="commentaireTexteMsg"></span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default" name="sendCommentaire">Envoyer</button>
                    </div>
                </form>

                <?php if ($commentaireManager->countCommentaires($id) != 0){ ?>
                    <?php foreach ($commentaireManager->getListCommentaires($id) as $commentaires){ ?>
                        <ul class="media-list comments">
                            <li class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object img-circle img-thumbnail" src="http://snipplicious.com/images/guest.png" width="64" alt="Generic placeholder image">
                                </a>
                                <?php if(isset($_SESSION['id_membre']) != '') { ?>
                                    <?php if ($membreManager->getMembre($_SESSION['id_membre'])->getRang() == 1) { ?>

                                        <div class="dropdown pull-right">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                Modération
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                                <li><a href="article.php?id=<?= $articleManager->getArticle($id)->getId(); ?>&commentaireDel=<?= $commentaires->getId_commentaire(); ?>"><i class="glyphicon  glyphicon-trash"></i> Supprimer</a></li>
                                                <li><a href="article.php?id=<?= $articleManager->getArticle($id)->getId(); ?>&moderationSpam=<?= $commentaires->getId_commentaire(); ?>"><i class="glyphicon  glyphicon-repeat"></i> Spam</a></li>
                                                <li><a href="article.php?id=<?= $articleManager->getArticle($id)->getId(); ?>&moderationInsulte=<?= $commentaires->getId_commentaire(); ?>"><i class="glyphicon  glyphicon-warning-sign"></i> Insulte</a></li>

                                            </ul>
                                        </div>
                                    <?php } ?>
                                <?php } ?>

                                <div class="media-body">
                                    <h5 class="media-heading pull-left"><?= htmlentities($commentaires->getAuteur()); ?></h5>
                                    <div class="comment-info pull-left">
                                        <div class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Sent from ***0.0.1"><i class="glyphicon  glyphicon-user"></i></div>
                                        <div class="btn btn-primary btn-xs"><a class="glyphicon  glyphicon-envelope white" href=""></a></div>
                                        <div class="btn btn-default btn-xs"><i class="glyphicon  glyphicon-calendar"></i> <?= $commentaires->getDate_ajout(); ?></div>
                                    </div>
                                    <br class="clearfix">
                                    <p class="well"><?= htmlentities($commentaires->getCommentaire()); ?></p>
                                </div>
                            </li>
                        </ul>
                    <?php } ?>
                <?php }else{ ?>
                    <div class="text-center">Il n'y a aucun commentaire</div>
                <?php } ?>

            </div>


        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modifArticle" tabindex="-1" role="dialog" aria-labelledby="modifArticleLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modifier l'article</h4>
                </div>
                <form action="#" method="post">
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="titre">Titre</label>
                            <input type="text" class="form-control" id="titre" name="titre" placeholder="Titre de l'article" value="<?= $articleManager->getArticle($id)->getTitre(); ?>">
                            <span class="msg-erreur" id="titreMsg"></span>
                        </div>
                        <div class="form-group">
                            <label for="contenu">Contenu</label>
                            <textarea class="form-control" id="contenu" name="contenu" rows="3"><?= $articleManager->getArticle($id)->getContenu(); ?></textarea>
                            <span class="msg-erreur" id="contenuMsg"></span>
                        </div>
                        <div class="form-group">
                            <label for="auteur">Auteur</label>
                            <input type="text" class="form-control" id="auteur" name="auteur" placeholder="Nom de l'auteur" value="<?= $articleManager->getArticle($id)->getAuteur(); ?>">
                            <span class="msg-erreur" id="auteurMsg"></span>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary" name="modif">Enregistrer</button>
                </form>
            </div>
        </div>
    </div>


<?php include 'footer.php'; ?>