<?php
/**
 * Connexion à la base de données
 */
try
{
    $bdd = new PDO('mysql:host=localhost;dbname=monblog;charset=utf8', 'root', '');
}
catch (Exception $e)
{
    exit('Erreur : ' . $e->getMessage());
}
include 'functions.php';
/**
 * @param $classe
 * Auto-load des classes
 */
function chargerClasse($classe)
{
    require 'class/' . $classe . '.php';
}

spl_autoload_register('chargerClasse');

session_start();

$membreManager = new MembresManager($bdd);
$articleManager = new ArticlesManager($bdd);
$commentaireManager = new CommentairesManager($bdd);
$App = new App();

?>