<?php

class Membre
{
    protected $_id_membre;
    protected $_pseudo;
    protected $_password;
    protected $_email;
    protected $_description;
    protected $_rang;
    protected $_valide;

    public function __construct(array $donnees)
    {
        $this->hydrate($donnees);
    }

    /**
     * Méthode pour l'hydratation des données
     * @param array $donnees
     */
    public function hydrate(array $donnees)
    {
        foreach ($donnees as $key => $value)
        {
            $methode  = 'set' . ucfirst($key);

            if (method_exists($this, $methode))
            {
                $this->$methode($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * @return mixed
     */
    public function getId_membre()
    {
        return $this->_id_membre;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->_pseudo;
    }

    /**
     * @return mixed
     */
    public function getRang()
    {
        return $this->_rang;
    }

    /**
     * @return mixed
     */
    public function getValide()
    {
        return $this->_valide;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->_email = $email;
    }

    /**
     * @param mixed $id_membre
     */
    public function setId_membre($id_membre)
    {
        $this->_id_membre = $id_membre;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->_password = $password;
    }

    /**
     * @param mixed $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->_pseudo = $pseudo;
    }

    /**
     * @param mixed $rang
     */
    public function setRang($rang)
    {
        $this->_rang = $rang;
    }

    /**
     * @param mixed $valide
     */
    public function setValide($valide)
    {
        $this->_valide = $valide;
    }
    
}