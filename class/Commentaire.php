<?php
class Commentaire
{
    private $_id_commentaire;
    private $_commentaire;
    private $_auteur;
    private $_date_ajout;
    private $_id_article;

    public function __construct(array $donnees)
    {
        $this->hydrate($donnees);
    }

    /**
     * Méthode pour l'hydratation des données
     * @param array $donnees
     */
    public function hydrate(array $donnees)
    {
        foreach ($donnees as $key => $value)
        {
            $methode  = 'set' . ucfirst($key);

            if (method_exists($this, $methode))
            {
                $this->$methode($value);
            }
        }
    }

    /**
     * @param mixed $auteur
     */
    public function setAuteur($auteur)
    {
        $this->_auteur = $auteur;
    }

    /**
     * @return mixed
     */
    public function getAuteur()
    {
        return $this->_auteur;
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->_commentaire;
    }

    /**
     * @return mixed
     */
    public function getDate_ajout()
    {
        return $this->_date_ajout;
    }

    /**
     * @return mixed
     */
    public function getId_article()
    {
        return $this->_id_article;
    }

    /**
     * @return mixed
     */
    public function getId_commentaire()
    {
        return $this->_id_commentaire;
    }

    /**
     * @param mixed $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->_commentaire = $commentaire;
    }

    /**
     * @param mixed $date_ajout
     */
    public function setDate_ajout($date_ajout)
    {
        $this->_date_ajout = $date_ajout;
    }

    /**
     * @param mixed $id_article
     */
    public function setId_article($id_article)
    {
        $this->_id_article = $id_article;
    }

    /**
     * @param mixed $id_commentaire
     */
    public function setId_commentaire($id_commentaire)
    {
        $this->_id_commentaire = $id_commentaire;
    }

}