<?php
class CommentairesManager
{

    private $_bdd;

    const MODERATION_SPAM = 'Le spam est interdit sur le site "Modifier par la modération"' ;
    const MODERATION_INSULTE = 'Pas d\'insulte ! "Modifier par la modération"';

    /**
     * ArticlesManager constructor.
     * @param $bdd
     */
    public function __construct($bdd)
    {
        $this->setDb($bdd);
    }

    /**
     * @param PDO $bdd
     */
    public function setDb(PDO $bdd)
    {
        $this->_bdd = $bdd;
    }

    /**
     * Compte le nombre de commentaire pour un article
     * @param $id
     * @return mixed
     */
    public function countCommentaires($id)
    {
        return $this->_bdd->query('SELECT COUNT(*) FROM commentaires WHERE id_article = ' . $id)->fetchColumn();
    }

    /**
     * Ajout de commentaire
     * @param Commentaire $commentaire
     */
    public function addCommentaire(Commentaire $commentaire)
    {
        $req = $this->_bdd->prepare('INSERT INTO commentaires(commentaire, auteur, date_ajout, id_article) VALUES(:commentaire, :auteur, NOW(), :id_article)');

        $req->bindValue(':commentaire', $commentaire->getCommentaire());
        $req->bindValue(':auteur', $commentaire->getAuteur());
        $req->bindValue(':id_article', $commentaire->getId_article());

        $req->execute();
        header('Location: article.php?id='.$commentaire->getId_article());
        die();

    }

    /**
     * Méthode pour afficher la liste des commentaires
     * @return array
     */
    public function getListCommentaires($id)
    {
        $commentaires = [];

        $req = $this->_bdd->prepare('SELECT id_commentaire, commentaire, auteur, DATE_FORMAT(date_ajout, \'%d/%m/%Y %Hh%imin%ss\') AS date_ajout FROM commentaires WHERE id_article = ? ORDER BY date_ajout');
        $req->execute(array($id));
        while ($donnees = $req->fetch(PDO::FETCH_ASSOC))
        {
            $commentaires[] = new Commentaire($donnees);
        }

        return $commentaires;
    }

    /**
     * Récupère un commentaire par rapport à son ID
     * @param $id
     * @return Article
     */
    public function getCommentaire($id)
    {
        $id = (int) $id;
        $req = $this->_bdd->query('SELECT id_commentaire, commentaire, auteur, DATE_FORMAT(date_ajout, \'%d/%m/%Y %Hh%imin%ss\') AS date_ajout FROM commentaires WHERE id_commentaire = '.$id);
        $donnees = $req->fetch(PDO::FETCH_ASSOC);

        return new Commentaire($donnees);
    }

    /**
     * Méthode pour supprimer un commentaire
     * @param $article
     */
    public function deleteCommentaire($commentaire, $id_article)
    {
        $this->_bdd->exec('DELETE FROM commentaires WHERE id_commentaire = ' . $commentaire);
        $_SESSION['delCommentaire'] = 'Le commentaire à bien été supprimé';
        header('Location: article.php?id=' . $id_article);
        die();
    }

    public function moderationSpam($moderationSpam, $msgModeration, $id_article)
    {

        $req = $this->_bdd->prepare('UPDATE commentaires SET commentaire = :commentaire WHERE id_commentaire = :id_commentaire');

        $req->bindValue(':commentaire', $msgModeration);
        $req->bindValue(':id_commentaire', $moderationSpam, PDO::PARAM_INT);

        $req->execute();
        header('Location: article.php?id=' . $id_article);
        die();
    }



}