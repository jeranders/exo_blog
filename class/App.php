<?php

class App
{
    /**
     * Méthode pour la gestion d'erreurs
     * @param $erreurs
     */
    public function erreurs($erreurs)
    {
        if (isset($erreurs) && count($erreurs) != 0)
        {
            echo '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>';
            foreach ($erreurs as $erreur)
            {
                echo $erreur .'<br>';
            }
            echo '</div>';
        }
    }

    public function erreursSession($erreurs)
    {
        if (isset($erreurs) && count($erreurs) != 0)
        {
            echo '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>';
            foreach ($erreurs as $erreur)
            {
                echo $erreur .'<br>';
            }
            echo '</div>';
        }
    }

    /**
     * Méthode pour afficher les message success
     * Exemple utilisation :
     * if (isset($_SESSION['msgSuccess']))
     * {
     *      $App->successSession($_SESSION['msgSuccess']);
     *      unset($_SESSION['msgSuccess']);
     * }
     * @param $messages
     */
    public function successSession($messages)
    {
        if (isset($messages) && count($messages) != 0)
        {
            echo '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>';

            echo $messages;

            echo '</div>';
        }
    }

    /**
     * Méthode qui vérifie l'exsistance d'une session membre
     */
    public function verifSession()
    {
        if(isset($_SESSION['id_membre']) != ''){
            header('Location: index.php');
            die();
        }
    }
}