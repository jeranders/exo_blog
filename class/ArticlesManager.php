<?php

class ArticlesManager
{
    private $_bdd;

    /**
     * ArticlesManager constructor.
     * @param $bdd
     */
    public function __construct($bdd)
    {
        $this->setDb($bdd);
    }

    /**
     * @param PDO $bdd
     */
    public function setDb(PDO $bdd)
    {
        $this->_bdd = $bdd;
    }

    /**
     * Méthode pour ajouter un article dans la base de données
     * @param Article $article
     */
    public function addArticle(Article $article)
    {
        $req = $this->_bdd->prepare('INSERT INTO articles(titre, date, contenu, auteur) VALUES(:titre, NOW(), :contenu, :auteur)');

        $req->bindValue(':titre', $article->getTitre());
        $req->bindValue(':contenu', $article->getContenu());
        $req->bindValue(':auteur', $article->getAuteur());

        $req->execute();
        header('Location: index.php');
        die();
    }

    /**
     * Méthode pour supprimer un article
     * @param Article $article
     */
    public function deleteArticle($article)
    {
        $this->_bdd->exec('DELETE FROM articles WHERE id = ' . $article);
        header('Location: index.php');
        die();
    }

    /**
     * Méthode pour compter le nombre d'articles
     * @return mixed
     */
    public function countArticle()
    {
        return $this->_bdd->query('SELECT COUNT(*) FROM articles')->fetchColumn();
    }

    /**
     * Méthode pour afficher la liste des articles
     * @return array
     */
    public function getListArticles()
    {
        $articles = [];

        $req = $this->_bdd->query('SELECT id, titre, DATE_FORMAT(date, \'%d/%m/%Y %Hh%imin%ss\') AS date, contenu, auteur FROM articles ORDER BY date DESC');

        while ($donnees = $req->fetch(PDO::FETCH_ASSOC))
        {
            $articles[] = new Article($donnees);
        }

        return $articles;
    }

    /**
     * Affiche un article par rapport à son ID
     * @param $id
     * @return Article
     */
    public function getArticle($id)
    {
        $id = (int) $id;
        $req = $this->_bdd->query('SELECT id, titre, date, contenu, auteur FROM articles WHERE id = '.$id);
        $donnees = $req->fetch(PDO::FETCH_ASSOC);

        return new Article($donnees);
    }

    /**
     * Mise à jour d'un article
     * @param Article $article
     */
    public function updateArticle(Article $article)
    {
        $req = $this->_bdd->prepare('UPDATE articles SET titre = :titre, contenu = :contenu, auteur = :auteur WHERE id = :id');

        $req->bindValue(':titre', $article->getTitre());
        $req->bindValue(':contenu', $article->getContenu());
        $req->bindValue(':auteur', $article->getAuteur());
        $req->bindValue(':id', $article->getId(), PDO::PARAM_INT);

        $req->execute();
        header('Location: article.php?id='.$article->getId());
        die();
    }

    
}