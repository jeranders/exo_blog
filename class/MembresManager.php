<?php

class MembresManager
{
    private $_bdd;

    /**
     * ArticlesManager constructor.
     * @param $bdd
     */
    public function __construct($bdd)
    {
        $this->setDb($bdd);
    }

    /**
     * @param PDO $bdd
     */
    public function setDb(PDO $bdd)
    {
        $this->_bdd = $bdd;
    }

    /**
     * Ajout de commentaire
     * @param Commentaire $commentaire
     */
    public function addMembre(Membre $membre)
    {
        $req = $this->_bdd->prepare('INSERT INTO membres(pseudo, password, email, description, rang, valide) VALUES(:pseudo, :password, :email, :description, :rang, :valide)');
        $req->bindValue(':pseudo',   $membre->getPseudo());
        $req->bindValue(':password', $membre->getPassword());
        $req->bindValue(':email',    $membre->getEmail());
        $req->bindValue(':description',    $membre->getDescription());
        $req->bindValue(':rang',     $membre->getRang());
        $req->bindValue(':valide',   $membre->getValide());
        $req->execute();
        header('Location: index.php');
        die();

    }

    /**
     * Méthode de connexion d'un membre
     * @param $connexion
     */
    public function connexion($connexion)
    {
        $erreursConnexion = [];
        $_SESSION['erreursConnexion'] = [];
        $req = $this->_bdd->prepare('SELECT * FROM membres WHERE pseudo = :pseudo');
        $req->bindValue(':pseudo',   $connexion->getPseudo());
        $req->execute();

        if($this->verifPseudo($connexion->getPseudo()) == 1)
        {
            $userinfo = $req->fetch();

            if ($userinfo && $this->bcrypt_verify_password($connexion->getPassword(), $userinfo['password']))
            {

                $_SESSION['id_membre'] = (int)$userinfo['id_membre'];
                $_SESSION['msgSuccess'] = 'Bonjour ' . $connexion->getPseudo();
                header('Location:index.php');
                die();

            }else
            {
                $erreursConnexion[] = 'Le mot de passe est incorrect';
                $_SESSION['erreursConnexion'][] = 'Le mot de passe est incorrect';

            }
        }else
        {
            $erreursConnexion[] = 'Le pseudo est incorrect';
            $_SESSION['erreursConnexion'][] = 'Le pseudo est incorrect';
        }
    }

    /**
     * Méthode qui vérifie si un pseudo est dans la bdd
     * @param $pseudo
     * @return mixed
     */
    public function verifPseudo($pseudo)
    {
        $req = $this->_bdd->prepare('SELECT * FROM membres WHERE pseudo = ?');
        $req->execute(array($pseudo));
        return $userexist = $req->rowCount();
        $req->closeCursor();
    }

    /**
     * Méthode pour cryper le mot de passe
     * @param $value
     * @param array $options
     * @return bool|string
     * @throws Exception
     */
    public function bcrypt_hash_password($value, $options = array()){

        $cost = isset($options['rounds']) ? $options['rounds'] : 10;
        $hash = password_hash($value, PASSWORD_BCRYPT, array('cost' => $cost));
        if ($hash === false)
        {
            throw new Exception("Bcrypt hashing n'est pas supporté.");
        }
        return $hash;
    }

    /**
     * Méthode pour vérifier le password
     * @param $value
     * @param $hashedValue
     * @return bool
     */
    public function bcrypt_verify_password($value, $hashedValue){
        return password_verify($value, $hashedValue);
    }

    /**
     * Affiche un membre par rapport à son ID
     * @param $id
     * @return Article
     */
    public function getMembre($id)
    {
        $id = (int) $id;
        $req = $this->_bdd->query('SELECT * FROM membres WHERE id_membre = ' . $id);
        $donnees = $req->fetch(PDO::FETCH_ASSOC);

        return new Membre($donnees);
    }
}