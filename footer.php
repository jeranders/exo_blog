
<!-- Modal -->
<div class="modal fade" id="ajoutArticle" tabindex="-1" role="dialog" aria-labelledby="ajoutArticleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ajouter un article</h4>
            </div>
            <form action="#" method="post">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="titre">Titre</label>
                        <input type="text" class="form-control" id="titre" name="titre" placeholder="Titre de l'article" value="<?php if (isset($_POST['titre'])){echo $_POST['titre'];}?>">
                        <span class="msg-erreur" id="titreMsg"></span>
                    </div>
                    <div class="form-group">
                        <label for="contenu">Contenu</label>
                        <textarea class="form-control" id="contenu" name="contenu" rows="3"><?php if (isset($_POST['contenu'])){echo $_POST['contenu'];}?></textarea>
                        <span class="msg-erreur" id="contenuMsg"></span>
                    </div>
                    <div class="form-group">
                        <label for="auteur">Auteur</label>
                        <input type="text" class="form-control" id="auteur" name="auteur" placeholder="Nom de l'auteur" <?php if (isset($_POST['auteur'])){echo $_POST['auteur'];}?>>
                        <span class="msg-erreur" id="auteurMsg"></span>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-primary" name="ajoutArticle">Enregistrer</button>
            </form>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="js/App.js"></script>
<script>
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    });
</script>
</body>
</html>