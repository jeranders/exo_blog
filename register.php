<?php
include 'bdd_autoload.php';
$App->verifSession();
/**
 * Gestion du formulaire d'inscription
 */
if (isset($_POST['sendInscription']))
{
    $pseudo = htmlentities($_POST['pseudo']);
    $password = $_POST['password'];
    $email = $_POST['email'];
    $description = 'Aucune description';
    $rang = (int) 2;
    $valide = (int) 1;
    $erreursInscription = [];

    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
    {
        $erreursInscription[] = "Adresse email invalide.";
    }

    if(strlen($_POST['pseudo']) <= 2)
    {
        if(strlen($_POST['pseudo']) == '')
        {
            $erreursInscription[] = 'Il n\'y a aucun pseudo';
        }else{
            $erreursInscription[] = 'Le pseudo est trop court';
        }
    }

    if (strlen($_POST['password']) <= 2)
    {
        if (strlen($_POST['password']) == '')
        {
            $erreursInscription[] = 'Il n\'y a aucun mot de passe';
        }else{
            $erreursInscription[] = 'Le mot de passe est trop court';
        }
    }

    if ($_POST['password'] != $_POST['rePassword'])
    {
        $erreursInscription[] = 'Les mots de passe sont différents';
    }

    if (count($erreursInscription) == 0)
    {
        $register = new Membre([
            'pseudo'         => $pseudo,
            'password'       => $membreManager->bcrypt_hash_password($password),
            'email'          => $email,
            'description'    => $description,
            'rang'           => $rang,
            'valide'         => $valide
        ]);

        $membreManager->addMembre($register);
    }
}

include 'header.php';
?>
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-md-offset-3">

                <h1>Inscription</h1>
                <?php
                if (isset($erreursInscription))
                {
                    $App->erreurs($erreursInscription);
                }
                ?>
                <form action="#" method="post">

                    <div class="form-group">
                        <label for="pseudo">Pseudo</label>
                        <input type="text" class="form-control" id="pseudo" name="pseudo" placeholder="Ex : Toto81">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Ex : toto81@gmail.com">
                    </div>

                    <div class="form-group">
                        <label for="password">Mot de passe</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>

                    <div class="form-group">
                        <label for="rePassword">Répéter le mot de passe</label>
                        <input type="password" class="form-control" id="rePassword" name="rePassword" placeholder="Password">
                    </div>

                    <button type="submit" name="sendInscription" class="btn btn-default">Valider</button>

                </form>
            </div>

        </div>
    </div>
<?php include 'footer.php'; ?>