$(function () {

    $('#titre').on('focus', function () {

        $('#titre').on('keyup', function ()
        {
            var currlength = $(this).val().length;

            if(currlength <= 2)
            {
                $('#titreMsg').text('Le titre doit avoir 3 caractères minimum');
            }else
            {
                 $('#titreMsg').text('');
            }
        });

    });

    $('#titre').on('blur', function () {

        var currlength = $(this).val().length;

        if(currlength == 0)
        {
            $('#titreMsg').text('');
        }

    });



    $('#auteur').on('focus', function () {

        $('#auteur').on('keyup', function ()
        {
            var currlength = $(this).val().length;

            if(currlength <= 2)
            {
                $('#auteurMsg').text('Le nom de l\'auteur doit avoir 3 caractères minimum');
            }else
            {
                 $('#auteurMsg').text('');
            }
        });

    });

    $('#auteur').on('blur', function () {

        var currlength = $(this).val().length;

        if(currlength == 0)
        {
            $('#auteurMsg').text('');
        }

    });


    $('#contenu').on('focus', function () {

        $('#contenu').on('keyup', function ()
        {
            var currlength = $(this).val().length;

            if(currlength <= 99)
            {
                $('#contenuMsg').text('Le contenu doit avoir 100 caractères minimum');
            }else
            {
                $('#contenuMsg').text('');
            }
        });

    });

    $('#contenu').on('blur', function () {

        var currlength = $(this).val().length;

        if(currlength == 0)
        {
            $('#contenuMsg').text('');
        }

    });


    /**
     * Commentaires
     */

    $('#auteurCommentaire').on('focus', function () {

        $('#auteurCommentaire').on('keyup', function ()
        {
            var currlength = $(this).val().length;

            if(currlength <= 2)
            {
                $('#auteurCommentaireMsg').text('Le nom de l\'auteur doit avoir 3 caractères minimum');
            }else
            {
                $('#auteurCommentaireMsg').text('');
            }
        });

    });

    $('#auteurCommentaire').on('blur', function () {

        var currlength = $(this).val().length;

        if(currlength == 0)
        {
            $('#auteurCommentaireMsg').text('');
        }

    });

    $('#commentaireTexte').on('focus', function () {

        $('#commentaireTexte').on('keyup', function ()
        {
            var currlength = $(this).val().length;

            if(currlength <= 99)
            {
                $('#commentaireTexteMsg').text('Le contenu doit avoir 100 caractères minimum');
            }else
            {
                $('#commentaireTexteMsg').text('');
            }
        });

    });

    $('#commentaireTexte').on('blur', function () {

        var currlength = $(this).val().length;

        if(currlength == 0)
        {
            $('#commentaireTexteMsg').text('');
        }

    });


    /**
     * Tooltip
     */

    $('[data-toggle="tooltip"]').tooltip();

});