-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 26 Août 2016 à 13:11
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `monblog`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `contenu` text NOT NULL,
  `auteur` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `articles`
--

INSERT INTO `articles` (`id`, `titre`, `date`, `contenu`, `auteur`) VALUES
(1, 'Mon article', '2016-08-18 21:56:40', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent interdum feugiat ex, ac scelerisque urna tincidunt sed. Etiam metus mauris, faucibus quis nibh vel, aliquet tristique arcu. Nam sit amet purus quam. Ut interdum urna vel felis rhoncus aliquet. Morbi elementum sapien magna, ac porta tortor molestie convallis. Suspendisse cursus sagittis ex a ornare. Cras laoreet blandit ante, eu consequat tortor pellentesque ut.', 'Moltes'),
(2, 'Mon titre', '2016-08-18 22:41:28', 'Vestibulum dapibus et erat vitae consequat. Duis egestas, ex ut bibendum scelerisque, leo diam consectetur ligula, ac aliquam orci turpis et sapien. Cras vel dolor tellus. Integer sit amet mi orci. Pellentesque nibh enim, semper in dictum tristique, faucibus id risus. Nullam sollicitudin ligula ut odio tincidunt varius. Sed sit amet lorem sed mauris sodales dapibus. Pellentesque ac nulla nisl. Nam tristique scelerisque tellus, et elementum eros. Sed et diam a tellus faucibus volutpat. Mauris pellentesque tortor tortor, at tincidunt felis mollis a. Sed ut leo aliquam risus lacinia aliquet sed a magna. Morbi semper justo ut lorem mollis, et molestie sem pretium. Quisque mollis nunc ligula, at mattis metus hendrerit nec. Quisque eget odio nec velit imperdiet aliquam. Cras vel commodo massa, in pulvinar urna.', 'Oldae');

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE `commentaires` (
  `id_commentaire` int(11) NOT NULL,
  `commentaire` text NOT NULL,
  `auteur` varchar(255) NOT NULL,
  `date_ajout` datetime NOT NULL,
  `id_article` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `commentaires`
--

INSERT INTO `commentaires` (`id_commentaire`, `commentaire`, `auteur`, `date_ajout`, `id_article`) VALUES
(1, 'Curabitur sodales urna sit amet dictum pretium. Morbi porttitor orci ligula, vel molestie purus aliquam sit amet. Morbi ex arcu, rutrum a fermentum id, elementum ac erat. Nulla facilisi. Pellentesque blandit eros vitae felis vehicula, nec tempus lectus feugiat. Cras vitae lorem in ipsum vehicula gravida sed in mauris. Sed eu augue vitae urna rhoncus ullamcorper a eu turpis. Fusce et massa eget lectus mollis scelerisque vel sed ex. In sodales, nisl eu mollis dictum, lectus ex mollis nibh, ac rutrum felis velit eu est. Phasellus consequat justo vitae nunc scelerisque vulputate. Duis molestie nisl sed congue posuere. Nunc rhoncus elit volutpat, tincidunt mi vel, faucibus augue. Aenean accumsan tincidunt tortor vel mattis. Praesent malesuada varius bibendum. Integer efficitur lacinia scelerisque. Sed eget finibus ex, et sodales nisi.', 'Jeranders', '2016-08-19 16:34:29', 1),
(2, 'Aenean luctus, justo quis molestie pretium, tortor libero placerat neque, vitae vulputate ante ex sed dolor. Donec sodales massa ac dolor mollis, eu sodales augue congue. Sed dignissim eros at condimentum faucibus. Proin consequat volutpat arcu, eu laoreet enim aliquet non. Integer sem arcu, elementum vel cursus at, placerat ac tellus. Fusce sit amet pretium arcu, ac porttitor felis. Duis sed bibendum nunc, rutrum hendrerit ipsum. Mauris sagittis tincidunt sapien at malesuada. Nam libero ipsum, fringilla in odio et, ullamcorper accumsan eros. Sed fringilla magna in volutpat dictum. Fusce dolor felis, mollis eget dapibus vel, blandit sit amet lacus. Donec justo turpis, sagittis a ornare vitae, maximus ac elit. Ut et augue sagittis, tempor ligula a, porttitor erat. Morbi ac porttitor ante.', 'AnCat', '2016-08-19 13:31:29', 1),
(3, 'Pellentesque iaculis erat laoreet lorem bibendum dapibus. Fusce interdum lectus eu ligula accumsan, a porttitor lacus euismod. Praesent vestibulum auctor aliquet. Vestibulum mauris purus, consequat sit amet augue eu, posuere accumsan neque. Quisque sollicitudin vehicula diam, quis consectetur neque. Mauris quis convallis metus. Maecenas ac faucibus massa, porta ornare ante. Aliquam erat volutpat.', 'Sony2k', '2016-08-25 10:31:34', 1),
(5, 'Pas d\'insulte ! "Modifier par la modération"', 'Robert', '2016-08-19 10:45:28', 2),
(6, 'Pas d\'insulte ! "Modifier par la modération"', 'Jeranders', '2016-08-19 10:46:40', 2),
(9, 'Le spam est interdit sur le site "Modifier par la modération"', 'JeanEd', '2016-08-26 11:19:08', 2);

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

CREATE TABLE `membres` (
  `id_membre` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `rang` int(11) NOT NULL,
  `valide` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `membres`
--

INSERT INTO `membres` (`id_membre`, `pseudo`, `password`, `email`, `description`, `rang`, `valide`) VALUES
(1, 'Jeranders', '$2y$10$doQWf22UgivLlUyrLOSGU.e7ODIWU50f.q3ldXHkLohuTIJVg/Uvy', 'jeranders@gmail.com', 'Aucune description', 1, 1),
(2, 'Ancat', '$2y$10$7DRFTFaMkmbqHOrzWz93Q.0T8hcafHHx53RvNXLs1AffAnld6Tbn6', 'ancat@gmail.com', 'Aucune description', 2, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD PRIMARY KEY (`id_commentaire`);

--
-- Index pour la table `membres`
--
ALTER TABLE `membres`
  ADD PRIMARY KEY (`id_membre`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `commentaires`
--
ALTER TABLE `commentaires`
  MODIFY `id_commentaire` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `membres`
--
ALTER TABLE `membres`
  MODIFY `id_membre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
