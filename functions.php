<?php
function setFlash($message, $type = 'success'){
    $_SESSION['flash']['message'] = $message;
    $_SESSION['flash']['type'] = $type;
}

function flash()
{
    if (isset($_SESSION['flash'])) {
        extract($_SESSION['flash']);
        unset($_SESSION['flash']);
        return "<div class='alert alert-$type text-center' role='alert'>
        <button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
        $message
        </div>";
    }
}